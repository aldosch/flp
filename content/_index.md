---
title: Focal Length photography
date: 2018-27-007T00:00:00+10:00
draft: false
description: Photography services for every occasion.
header:
  description: Photography services for every occasion. Let's capture a beautiful moment.
  image:
    url: img/home_img.jpg
    alt_text: 
    responsive_sources:
      "848": img/home_848x443.png
      "565": img/home_565x420.png
      "360": img/home_360x318.png
text_groups:
  - name: Intro
    description: Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quia esse aut, blanditiis ullam. Similique quo iusto ipsa, excepturi autem voluptate consectetur sit quae praesentium <span class="default-text bold-text">aliquam molestiae</span> minima ex perferendis aut unde tempora amet esse inventore quaerat molestias eum distinctio eligendi ducimus. Commodi voluptatem ab debitis inventore, laborum at maiores reiciendis dolor deleniti ipsam distinctio voluptas eos autem quidem a quia laboriosam similique soluta rem? Eum.
projects:
  - title: Strato
    type: wed design
    link: https://unsplash.com/photos/hpjSkU2UYSU
    image:
      url: img/strato_1130x590.jpg
      alt_text: The Strato web design theme
      responsive_sources:
        "848": img/strato_848x443.jpg
        "565": img/strato_565x420.jpg
        "360": img/strato_360x318.jpg
  - title: Analytik
    type: UI/UX
    link: https://unsplash.com/photos/yeB9jDmHm6M
    class: short-col
    image:
      url: img/analytik_364x590.jpg
      alt_text: The Analytic web design theme
      responsive_sources:
        "848": img/analytik_848x443.jpg
        "565": img/analytik_565x420.jpg
        "360": img/analytik_360x318.jpg
  - title: Friends
    type: web design
    link: https://unsplash.com/photos/ir5lIkVFqC4
    class: wide-col
    image:
      url: img/friends_746x590.jpg
      alt_text: The Friends theme
      responsive_sources:
        "848": img/friends_848x443.jpg
        "565": img/friends_565x420.jpg
        "360": img/friends_360x318.jpg
  - title: Food
    type: web design
    link: https://unsplash.com/photos/JVSgcV8_vb4
    class: wide-col
    image:
      url: img/food_746x590.jpg
      alt_text: The Food website for recipes
      responsive_sources:
        "848": img/food_848x443.jpg
        "565": img/food_565x420.jpg
        "360": img/food_360x318.jpg
  - title: StatApp
    type: UI/UX
    link: https://unsplash.com/photos/nJX74kn1yn4
    class: short-col
    image:
      url: img/statapp_364x590.jpg
      alt_text: The application for statistic
      responsive_sources:
        "848": img/statapp_848x443.jpg
        "565": img/statapp_565x420.jpg
        "360": img/statapp_360x318.jpg
---